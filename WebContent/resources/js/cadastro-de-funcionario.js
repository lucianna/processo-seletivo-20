var cadastro = new Vue({
	el:"#cadastro",
    data: {
		listaSetor:[],
        FormData: []
    },
    created: function(){
        let vm =  this;
        vm.listarSetores();
    },
    methods:{
		listarSetores: function(){
			const vm = this;
			axios.get("/funcionarios/rest/setores")
			.then(response => {
				vm.listaSetor = response.data;
				console.log(vm.listaSetor);
		}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar Setoes");
			}).finally(function() {
			});
		},
	//Busca os itens para a lista da primeira página
        save: function(){
			const vm = this;
			axios.post("/funcionarios/rest/funcionarios/")
			.then(response => {vm.funcion = response.data; 		
		}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi possível listar natureza de serviços");
			}).finally(function() {
			});
		},
		mostraAlertaErro: function(erro, mensagem){
			console.log(erro);
			alert(mensagem);
		}
    }
});