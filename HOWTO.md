# processo-seletivo Luaciana Rodrigues Carvalho de Souza

# Seguem links para testes

http://localhost:8080/funcionarios/index.html

http://localhost:8080/funcionarios/pages/novo-funcionario.html

http://localhost:8080/funcionarios/rest/funcionarios/

http://localhost:8080/funcionarios/rest/setores/

# Testes

Teste de criação de funcionário a partir do MySQL;

Configuração do FuncionarioService para create, delete e update dos novos funcionários cadastrados;

Configuração do github no windows para fazer commit;

Testes realizados na comunicação com o banco de dados através do sistema postman.

Teste de CRUD completo e funcionando corretamente.

Criação de service, DAO, método listarSetores no no JS do funcionário do Setor.

# Requisitos

1. Para configurar o computador
2. Instalação do GIT CMD = Git Bash. Clone do código;
3. Instalação do JDK8;
4. Instalação do Tomcat 9; Zip, coloca na pasta do projeto e export.
5. Instalação do eclipse; Importar o projeto clonado, Configuração do Servers (servidor), maven project existente.
6. Instalação do workbench mysql para o banco de dados.persistence.xml
7. Instalação do MySQLe Configuração do db;
8. Instalação do Node js;


